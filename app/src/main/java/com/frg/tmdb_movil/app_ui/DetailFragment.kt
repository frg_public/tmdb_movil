package com.frg.tmdb_movil.app_ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.frg.tmdb_movil.BR
import com.frg.tmdb_movil.app_viewmodel.DetailViewModel
import com.frg.tmdb_movil.databinding.DetailFragmentViewBinding
import com.frg.tmdb_movil.entities.model.MovieDetail
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment(private val id: Int): Fragment() {

    private val viewModel: DetailViewModel by viewModels<DetailViewModel>()
    private lateinit var binding: DetailFragmentViewBinding
    private var movieDetail: MovieDetail? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        binding = DetailFragmentViewBinding.inflate(inflater)
        viewModel.getDetail(id)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUI()
        viewModel.response.observe(this.viewLifecycleOwner) {
            movieDetail = it
            onUpdated()
        }
    }

    private fun setUpUI() {
        binding.swipe.setOnRefreshListener {
            viewModel.getDetail(id)
        }
    }
    private fun onUpdated() {
        movieDetail?.let {
            binding.swipe.isRefreshing = false
            binding.setVariable(BR.detail, it)
            binding.executePendingBindings()
        }
    }
}