package com.frg.tmdb_movil.app_ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.frg.tmdb_movil.R
import com.frg.tmdb_movil.app_viewmodel.LoginViewModel
import com.frg.tmdb_movil.databinding.LoginFragmentViewBinding
import com.frg.tmdb_movil.entities.listeners.OnLoginListener
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment: Fragment(), OnLoginListener {

    private lateinit var binding: LoginFragmentViewBinding

    private val viewModel: LoginViewModel by activityViewModels<LoginViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = LoginFragmentViewBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpBehavior()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setUpBehavior(){
        viewModel.addListener(this)
        binding.loginButton.setOnClickListener {
            loginWithCredentials()
        }

        binding.loginAnonymously.setOnClickListener {
            loginAnonymously()
        }
    }

    private fun loginWithCredentials() {
        val user = binding.userInput.text?.toString().orEmpty()
        val password = binding.passInput.text?.toString().orEmpty()
        if(user.isNotBlank() && password.isNotBlank()) {
            enableInputs(false)
            viewModel.loginWithCredentials(user, password)
        } else {
            this.context?.resources?.getString(R.string.login_empty_inputs)?.let {
                Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show()
            }
        }
    }
    private fun loginAnonymously() {
        enableInputs(false)
        viewModel.loginAnonymously()
    }

    private fun enableInputs(enabled: Boolean) {
        binding.userInput.isEnabled = enabled
        binding.passInput.isEnabled = enabled
        binding.loginButton.isEnabled = enabled
        binding.loginAnonymously.isEnabled = enabled
        binding.progressBar.visibility = if(enabled) View.GONE else View.VISIBLE
    }
    override fun onLoginSuccess() {
        this.onDestroy()
    }

    override fun onLoginFailure() {
        enableInputs(true)
        this.context?.resources?.getString(R.string.login_invalid_creds)?.let {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show()
        }
    }
}