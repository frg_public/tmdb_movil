package com.frg.tmdb_movil.app_ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.frg.tmdb_movil.app_viewmodel.NowPlayingViewModel
import com.frg.tmdb_movil.core_usecase.adapters.NowPlayingAdapter
import com.frg.tmdb_movil.databinding.NowplayingFragmentViewBinding
import com.frg.tmdb_movil.entities.listeners.OnMovieSelected
import com.frg.tmdb_movil.entities.model.NowPlaying
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NowPlayingFragment: Fragment() {

    private val viewModel: NowPlayingViewModel by activityViewModels<NowPlayingViewModel>()
    private lateinit var binding: NowplayingFragmentViewBinding
    private var items: List<NowPlaying.Results> = listOf()
    private lateinit var adapter: NowPlayingAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = NowplayingFragmentViewBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpUI()
        binding.swipe.isRefreshing = true
        viewModel.response.observe(this.viewLifecycleOwner) {
            binding.swipe.isRefreshing = false
            items = it.results.orEmpty()
            updateAdapter()
        }
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setUpUI(){
        binding.swipe.setOnRefreshListener {
            viewModel.getMainList()
        }

        adapter = NowPlayingAdapter(items, object : OnMovieSelected {
            override fun onMovieSelected(movie: NowPlaying.Results) {
                viewModel.onMovieSelected(movie)
            }
        })
        binding.collection.adapter = adapter
    }

    private fun updateAdapter() {
        adapter.updateContent(items)
    }
}