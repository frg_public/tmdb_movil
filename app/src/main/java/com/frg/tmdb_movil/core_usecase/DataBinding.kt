package com.frg.tmdb_movil.core_usecase

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.frg.tmdb_movil.entities.network.AppMockup

/**
 * Glide images
 */
@BindingAdapter("path")
fun setImage(view: ImageView, path: String?){
    path?.let {
        val imagePath = AppMockup.COLLECTION_PATH+path
        Glide.with(view.context).load(imagePath).into(view)
    }
}


class DataBinding