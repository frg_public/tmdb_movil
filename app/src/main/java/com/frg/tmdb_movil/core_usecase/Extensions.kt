package com.frg.tmdb_movil.core_usecase

import android.content.Context
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


/**
 * Extension function
 */
fun String.dateFormat(context: Context): String{
    val timestamp = System.nanoTime()
    //DATE FORMAT: "2024-03-27",
    return this
}
/**
 * Hash MD5 function. Returns a string representation of the MD5 byte conversion of a string.
 */
fun String.hash(): String {
    val sb = StringBuilder()
    return try {
        val temp = MessageDigest.getInstance("MD5").digest(this.toByteArray(StandardCharsets.UTF_8))
        temp.forEach { sb.append(String.format("%02x", it)) }

        sb.toString()
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
        sb.append(Integer.toHexString(this.hashCode()).uppercase())
        sb.toString()
    }
}