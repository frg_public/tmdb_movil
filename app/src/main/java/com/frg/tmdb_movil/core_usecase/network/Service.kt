package com.frg.tmdb_movil.core_usecase.network

import com.frg.tmdb_movil.entities.network.ApiService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


/**
 * Retrofit injection.
 */
@Module
@InstallIn(SingletonComponent::class)
object Service {

    /**
     * Dummy URL.
     */
    @Provides
    fun provideBaseUrl() = "https://postman-echo.com/"


    /**
     * Provides OkHttpClient to retrofit
     * Add here: listeners and Interceptors.
     */
    @Singleton
    @Provides
    fun provideClient(): OkHttpClient =
        OkHttpClient.Builder()
            .eventListenerFactory(NetworkEvents.NetworkFactory())
            .build()


    /**
     * Provides Moshi to Retrofit.
     * Add adapters and converters here
     */
    @Singleton
    @Provides
    fun provideMoshi(): Moshi =
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()


    /**
     * Provides retrofit service.
     */
    @Singleton
    @Provides
    fun provideRetrofit(baseUrl: String, okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .baseUrl(baseUrl)
            .build()
    }

    /**
     * Provides ApiService to application
     */
    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

}