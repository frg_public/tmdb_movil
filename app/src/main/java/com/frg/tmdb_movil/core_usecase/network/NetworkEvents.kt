package com.frg.tmdb_movil.core_usecase.network

import android.util.Log
import okhttp3.Call
import okhttp3.EventListener
import java.util.*

/**
 * OkHttp Event Listener.
 * To log the url requested and response time.
 * No interceptor needed for now.
 */
class NetworkEvents : EventListener() {


    /**
     * Timestamp to register the beginning of the call
     */
    private var callBegin: Long = 0L

    override fun callStart(call: Call) {
        callBegin = System.nanoTime()
    }

    override fun responseBodyEnd(call: Call, byteCount: Long) {
        val responseBodyEnd = System.nanoTime()
        var velocity = -1.0
        val deltaT: Double = (responseBodyEnd - callBegin) / 1e6
        if (deltaT > 0) {
            velocity = byteCount / deltaT
        }
        Log.d("NETWORK_RESPONSE>",
            String.format(
                Locale.US,
                "%n %s %nin  %.1fms%n%d Bytes%n vel::%.9fKB/s",
                call.request().url(),
                deltaT,
                byteCount,
                velocity
            )
        )
    }


    /**
     * NetworkFactoryBuilder to return an instance of NetworkEvents
     */
    class NetworkFactory : Factory {
        override fun create(call: Call): EventListener {
            return NetworkEvents()
        }
    }
}