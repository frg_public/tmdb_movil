package com.frg.tmdb_movil.core_usecase.network

import com.frg.tmdb_movil.entities.model.MovieDetail
import com.frg.tmdb_movil.entities.model.NowPlaying
import com.frg.tmdb_movil.entities.network.ApiService
import com.squareup.moshi.JsonDataException
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

/**
 * Implementation of ApiService.
 * It handles calls to Retrofit Service
 * @see Service
 */
class Repository @Inject constructor(private val service : ApiService) {

    /**
     */
    suspend fun getPlayingNow(url: String): NowPlaying? {
        return try {
            service.getPlayingNow(url)
        } catch (e: HttpException) {
            e.printStackTrace()
            null
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
            null
        } catch (e: JsonDataException) {
            e.printStackTrace()
            null
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }

    suspend fun getMovieDetail(url: String): MovieDetail? {
        return try {
            service.getMovieDetail(url)
        } catch (e: HttpException) {
            e.printStackTrace()
            null
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
            null
        } catch (e: JsonDataException) {
            e.printStackTrace()
            null
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }
}