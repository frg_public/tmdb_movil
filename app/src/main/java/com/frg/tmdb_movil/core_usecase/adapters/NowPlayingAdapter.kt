package com.frg.tmdb_movil.core_usecase.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.frg.tmdb_movil.BR
import com.frg.tmdb_movil.R
import com.frg.tmdb_movil.entities.listeners.OnMovieSelected
import com.frg.tmdb_movil.entities.model.NowPlaying

class NowPlayingAdapter(private var items: List<NowPlaying.Results>, private val onMovieSelected: OnMovieSelected): RecyclerView.Adapter<NowPlayingAdapter.MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            viewType,
            parent,
            false
        )
        return MovieViewHolder(binding)
    }

    fun updateContent(newItems: List<NowPlaying.Results>) {
        this.items = newItems
        notifyItemRangeChanged(0, itemCount)
    }
    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_movie
    }
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        if(position in 0..items.size) {
            holder.setContent(items[position])
            holder.replaceListener(onMovieSelected)
            holder.onUpdated()
        }
    }

    class MovieViewHolder(private val binding: ViewDataBinding): ViewHolder(binding.root) {

        private var content: NowPlaying.Results? = null
        private var _onMovieSelected: OnMovieSelected = object : OnMovieSelected { override fun onMovieSelected(movie: NowPlaying.Results) { } }
        private val onMovieSelected: OnMovieSelected get() {
            return _onMovieSelected
        }
        fun setContent(item: NowPlaying.Results) {
            this.content = item
            binding.root.setOnClickListener {
                content?.let { onMovieSelected.onMovieSelected(it) }
            }
            onUpdated()
        }
        fun replaceListener(listener: OnMovieSelected) {
            this._onMovieSelected = listener
        }


        fun onUpdated() {
            binding.setVariable(BR.movie, content)
            binding.executePendingBindings()
        }
    }

}