package com.frg.tmdb_movil.app_viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.frg.tmdb_movil.core_usecase.network.Repository
import com.frg.tmdb_movil.entities.listeners.OnMovieSelected
import com.frg.tmdb_movil.entities.model.NowPlaying
import com.frg.tmdb_movil.entities.network.AppMockup
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NowPlayingViewModel @Inject constructor(private val repository: Repository): ViewModel(), OnMovieSelected {

    private val _response = MutableLiveData<NowPlaying>()

    private var onMovieSelected: OnMovieSelected? = null
    val response: MutableLiveData<NowPlaying> get() { return _response }
    fun getMainList(){
        CoroutineScope(Dispatchers.IO).launch {
            val baseUrl = AppMockup.BASE_URL+AppMockup.PUBLIC_API_KEY
            repository.getPlayingNow(baseUrl)?.let {
                _response.postValue(it)
            }
        }
    }

    fun setOnMovieSelected(listener: OnMovieSelected){
        onMovieSelected = listener
    }

    override fun onMovieSelected(movie: NowPlaying.Results) {
        movie.id?.let { onMovieSelected?.onMovieSelected(movie) }
    }

}