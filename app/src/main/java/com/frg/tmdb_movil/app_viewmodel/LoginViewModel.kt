package com.frg.tmdb_movil.app_viewmodel

import androidx.lifecycle.ViewModel
import com.frg.tmdb_movil.entities.listeners.OnLoginListener
import com.frg.tmdb_movil.entities.listeners.OnSessionListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(): ViewModel() {

    private var loginListeners: OnLoginListener? = null
    private var sessionListener: OnSessionListener? = null
    private var authentication: FirebaseAuth? = null
    fun loginWithCredentials(user: String, pass: String) {
        Firebase.auth.signInWithEmailAndPassword(user, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) sendOnLoginSuccess() else  sendOnSessionClosed()
            }
    }

    fun loginAnonymously() {
            Firebase.auth.signInAnonymously().addOnCompleteListener { task ->
                if (task.isSuccessful) sendOnLoginSuccess() else sendOnSessionClosed()
            }
    }
    fun checkSession() {
        val currentUser = Firebase.auth.currentUser
        if(currentUser == null) sendOnLoginFailure() else sendOnSessionClosed()
    }

    fun singOut() {
        Firebase.auth.signOut()
        sendOnSessionClosed()
    }

    fun addListener(listener: OnLoginListener) {
        loginListeners = listener
    }
    fun addListener(listener: OnSessionListener) {
        sessionListener = listener
    }

    fun setAuth(auth: FirebaseAuth){
        this.authentication = auth
    }
    private fun sendOnLoginSuccess() {
        loginListeners?.onLoginSuccess()
        sessionListener?.onLoginSuccess()
    }

    private fun sendOnLoginFailure(){
        loginListeners?.onLoginFailure()
    }

    private fun sendOnSessionClosed() {
        sessionListener?.onSessionClosed()
    }

}