package com.frg.tmdb_movil.app_viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.frg.tmdb_movil.core_usecase.network.Repository
import com.frg.tmdb_movil.entities.model.MovieDetail
import com.frg.tmdb_movil.entities.model.NowPlaying
import com.frg.tmdb_movil.entities.network.AppMockup
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

    @HiltViewModel
class DetailViewModel @Inject constructor(private val repository: Repository): ViewModel() {

        private val _response = MutableLiveData<MovieDetail>()
        val response: MutableLiveData<MovieDetail> get() { return _response }
        fun getDetail(id: Int){
            CoroutineScope(Dispatchers.IO).launch {
                val baseUrl = AppMockup.MOVIE_PATH+id+AppMockup.PUBLIC_API_KEY
                repository.getMovieDetail(baseUrl)?.let {
                    _response.postValue(it)
                }
            }
        }
}