package com.frg.tmdb_movil.entities.network

import com.frg.tmdb_movil.entities.model.MovieDetail
import com.frg.tmdb_movil.entities.model.NowPlaying
import retrofit2.http.*

/**
 * Api Service interface to retrofit
 */
interface ApiService {


    /**
     * To get a CharacterResponse object from the URL provided.
     */
    @GET
    suspend fun getPlayingNow(@Url url: String): NowPlaying

    @GET
    suspend fun getMovieDetail(@Url url: String): MovieDetail
}