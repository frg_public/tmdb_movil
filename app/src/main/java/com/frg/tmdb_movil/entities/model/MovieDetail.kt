package com.frg.tmdb_movil.entities.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetail(
    // Imagen, título, duración, fecha de estreno,
    //clasificación, géneros y descripción.
    @field:Json(name= "adult") val adult: Boolean?,
    @field:Json(name= "backdrop_path") val backdrop_path: String?,
    @field:Json(name= "genres") val genres: List<Genres>?,
    @field:Json(name= "homepage") val homepage: String?,
    @field:Json(name= "id") val id: String?,
    @field:Json(name= "imdb_id") val imdb_id: String?,
    @field:Json(name= "original_title") val original_title: String?,
    @field:Json(name= "overview") val overview: String?,
    @field:Json(name= "poster_path") val poster_path: String?,
    @field:Json(name= "release_date") val release_date: String?,
    @field:Json(name= "status") val status: String?,
    @field:Json(name= "title") val title: String?,
): Parcelable {
    @Parcelize
    data class BelongsToCollection(
        @field:Json(name= "id") val id: Int?,
        @field:Json(name= "name") val name: String?,
        @field:Json(name= "poster_path") val posterPath: String?,
        @field:Json(name= "backdrop_path") val backdropPath: String?
    ):Parcelable

    @Parcelize
    data class Genres(
        @field:Json(name = "id") val id: Int?,
        @field:Json(name = "name") val name: String?
    ): Parcelable

    @Parcelize
    data class ProductionCompanies(
        @field:Json(name = "id") val id: Int?,
        @field:Json(name = "logo_path") val logoPath: String?,
        @field:Json(name = "name") val name: String?,
        @field:Json(name = "origin_country") val originCountry: String?,
    ):Parcelable

    @Parcelize
    data class ProductionCountries(
        @field:Json(name = "iso_3166_1") val iso1661: String?,
        @field:Json(name = "name") val name: String?,
    ):Parcelable

    @Parcelize
    data class SpokenLanguages(
        @field:Json(name = "english_name") val englishName: String?,
        @field:Json(name = "iso_3166_1") val iso1661: String?,
        @field:Json(name = "name") val name: String?,
    ):Parcelable

}