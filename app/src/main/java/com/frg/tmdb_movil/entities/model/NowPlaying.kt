package com.frg.tmdb_movil.entities.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class NowPlaying(
    @field:Json(name="dates") val dates: Dates?,
    @field:Json(name="page") val page: Int?,
    @field:Json(name="results") val results: List<Results>?,
): Parcelable {
    @Parcelize
    data class Dates(
        @field:Json(name="maximum") val maximum: String?,
        @field:Json(name="minimum") val minimum: String?,
    ):Parcelable
    @Parcelize
    data class Results(
        @field:Json(name="backdrop_path") val backdrop_path: String?,
        @field:Json(name="id") val id: Int?,
        @field:Json(name="overview") val overview: String?,
        @field:Json(name="popularity") val popularity: Float?,
        @field:Json(name="poster_path") val poster_path: String?,
        @field:Json(name="title") val title: String?,
        @field:Json(name="vote_average") val vote_average: Float?,
        @field:Json(name="vote_count") val vote_count: Int?
    ) : Parcelable
}