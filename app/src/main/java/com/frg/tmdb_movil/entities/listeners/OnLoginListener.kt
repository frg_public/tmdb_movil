package com.frg.tmdb_movil.entities.listeners

interface OnLoginListener {
    fun onLoginSuccess()
    fun onLoginFailure()
}