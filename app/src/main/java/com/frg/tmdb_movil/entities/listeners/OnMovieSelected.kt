package com.frg.tmdb_movil.entities.listeners

import com.frg.tmdb_movil.entities.model.NowPlaying

interface OnMovieSelected {

    fun onMovieSelected(movie: NowPlaying.Results)
}