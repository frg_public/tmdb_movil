package com.frg.tmdb_movil.entities.network

import android.content.Context

/**
 * TEMPORAL HARDCODE STATIC VALUES :: THIS PRACTICE ITS NOT RECOMMENDED
 * TODO: IMPLEMENT FIREBASE AND SHARED PREFERENCES TO REPLACE THIS
 */
class AppMockup {
    companion object {
        /**
         * Get From Firebase RemoteConfig
         */
        const val BASE_URL: String = "https://api.themoviedb.org/3/movie/now_playing"

        /**
         * Get From Firebase RemoteConfig
          */
        const val COLLECTION_PATH: String = "https://image.tmdb.org/t/p/original"

        /**
         * Get From Firebase RemoteConfig
          */
        const val MOVIE_PATH: String = "https://api.themoviedb.org/3/movie/"

        /**
         * Get From Firebase RemoteConfig
         */
        const val PUBLIC_API_KEY = "?api_key=c0823934438075d63f1dbda4023e76fc&language=es-MX"
        /**
         * Get From Firebase RemoteConfig
         */
        const val PUBLIC_ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNzYxZjE0NzI2ZTMwMWRhNGFhZGU0YzVjMjQwOTEzMyIsInN1YiI6IjY2Mzk4YWRhNjY1NjVhMDEyYzE2ZGFhYiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.vTUiEIG3oUYbV8ElTqBgTzSN3sVYWLpVapfGIBwivsk"

        const val HEADER_AUTH = "Authorization"
        const val HEADER_BEARER = "Bearer "
        /**
         * Temporal alternative to FirebaseRemoteConfig for default values
         * Temporary function to store variables to get from remote config
         * If you see this. I couldn't find the time to implement firebase.
        */
    }
}