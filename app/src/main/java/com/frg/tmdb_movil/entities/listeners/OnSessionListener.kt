package com.frg.tmdb_movil.entities.listeners

interface OnSessionListener {

    fun onLoginSuccess()
    fun onSessionClosed()

}