package com.frg.tmdb_movil

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.FragmentManager
import com.frg.tmdb_movil.app_ui.DetailFragment
import com.frg.tmdb_movil.app_ui.LoginFragment
import com.frg.tmdb_movil.app_ui.NowPlayingFragment
import com.frg.tmdb_movil.app_viewmodel.LoginViewModel
import com.frg.tmdb_movil.app_viewmodel.NowPlayingViewModel
import com.frg.tmdb_movil.databinding.ActivityMainBinding
import com.frg.tmdb_movil.entities.listeners.OnMovieSelected
import com.frg.tmdb_movil.entities.listeners.OnSessionListener
import com.frg.tmdb_movil.entities.model.NowPlaying
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), OnSessionListener, OnMovieSelected {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: LoginViewModel by viewModels<LoginViewModel>()
    private val listModel: NowPlayingViewModel by viewModels<NowPlayingViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        Firebase.auth
        viewModel.addListener(this)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.maLogout.setOnClickListener { viewModel.singOut() }
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.main) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }


    override fun onStart() {
        super.onStart()
        viewModel.checkSession()
    }

    override fun onDestroy() {
        viewModel.singOut()
        super.onDestroy()
    }

    private fun showLogin() {
        if(supportFragmentManager.fragments.isEmpty()) {
            supportFragmentManager.beginTransaction()
                .add(binding.container.id, LoginFragment())
                .commit()
        } else {
            supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            supportFragmentManager.beginTransaction()
                .replace(binding.container.id, LoginFragment())
                .commit()
        }
    }
    private fun loadMain() {
        listModel.getMainList()
        listModel.setOnMovieSelected(this)
        if(supportFragmentManager.fragments.isEmpty()) {
            supportFragmentManager.beginTransaction()
                .add(binding.container.id, NowPlayingFragment())
                .commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(binding.container.id, NowPlayingFragment())
                .commit()
        }
    }

    override fun onLoginSuccess() {
        loadMain()
    }

    override fun onSessionClosed() {
        showLogin()
    }

    override fun onMovieSelected(movie: NowPlaying.Results) {
        movie.id?.let {
        supportFragmentManager.beginTransaction()
            .add(binding.container.id, DetailFragment(it))
            .addToBackStack("$it")
            .commit()
        }
    }

}