# TMDB_movil

## Getting started

## Descripción
La siguiente prueba consiste en construir una aplicación móvil nativa que permita iniciar
sesión y que muestre un listado y su detalle.
## Recursos
• Lista: https://api.themoviedb.org/3/movie/now_playing
• Doc: https://developer.themoviedb.org/reference/intro/getting-started
## Instrucciones de la prueba:
1. La aplicación debe tener un inicio de sesión
   a. Se sugiere autenticación con Firebase.
   b. Puedes utilizar usuario y contraseña estático.
   c. Extra: autenticación con redes sociales.
2. Al iniciar sesión deberás visualizar un mensaje de bienvenida en la pantalla
   principal y la lista de Peliculas.
   a. Mostrar nombre, imagen y calificación
   b. Extra: Se sugiere utilizar paginación y / o pull refesh al listado
3. Cada uno de los items de la lista deberá mostrar el detalle en otra vista.
   a. Mostrar la siguiente información: Imagen, título, duración, fecha de estreno,
   clasificación, géneros y descripción.
4. Cierre de sesión.
   Condiciones:
   • Tienes 24 horas para entregar la evaluación a su reclutador de RH, y enviar un
   correo electrónico a la cuenta del reclutador con copia a:
   carmen.can@macropay.mx
   • En el correo se deberá enviar el link del repositorio github del código fuente y
   adjuntar en un .zip el aplicativo .apk release para que se valide la prueba.
   • El diseño deberá mostrar conceptos de material design.
   Qué evaluaremos:
   • Usar lenguaje de programación Kotlin (de preferencia) o Java
   • Usar un patrón de diseño (MVVM, MVC, etc)
   • Uso de librerías
   • Mejores prácticas.
